﻿using System;
using System.IO;
using Aether.ProtocolBuilder.Core;

namespace Aether.ProtocolBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            ShowLogo();
            Startup.Run();
            Console.Read();
        }
        
        private static void ShowLogo()
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            string logo = @"                                  _____          __  .__                                              
                                 /  _  \   _____/  |_|  |__   ___________ 
                                /  /_\  \_/ __ \   __\  |  \_/ __ \_  __ \
                               /    |    \  ___/|  | |   Y  \  ___/|  | \/
                               \____|__  /\___  >__| |___|  /\___  >__|   
                                       \/     \/          \/     \/       proto-builder";

            int totalWidth = (Console.BufferWidth + logo.Length) / 2;
            Console.WriteLine(logo.PadLeft(totalWidth) + Environment.NewLine);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}