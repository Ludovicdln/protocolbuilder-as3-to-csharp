﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aether.ProtocolBuilder.IO.Files
{
    public class CsFile : IFile
    {
        private string _name;
        private int _protocolId;
        private CsFile _abstractClass;

        private IEnumerable<Field> _fields;
        public CsFile(string name, int protocolId, IEnumerable<Field> fields) => (_name, _protocolId, _fields) = (name, protocolId, fields);
        public CsFile(string name, int protocolId, IEnumerable<Field> fields, CsFile abstractClass) 
            => (_name, _protocolId, _fields, _abstractClass) = (name, protocolId, fields, abstractClass);
            
        public IEnumerable<string> Build()
        {
            List<string> contents = new List<string>();
            
            // NAMESPACE

            var nameSpace = $"namespace Aether.Protocol.Messages";
            var fieldsBuilder = new StringBuilder();
            var fieldsAbstractBuilder = new StringBuilder();
            
            contents.Add(nameSpace);
            contents.Add("{");
            
            // RECORD
            for (int i = 0; i < _fields.Count(); i++) {
                fieldsBuilder.Append(i < _fields.Count() -1 ? $"{_fields.ElementAt(i).Type} {_fields.ElementAt(i).Name}, " : $"{_fields.ElementAt(i).Type} {_fields.ElementAt(i).Name}");
            }

            if (_abstractClass != null)
            {
                if (_fields.Any()) fieldsBuilder.Append(", ");
                
                for (int i = 0; i <  _abstractClass._fields.Count(); i++)
                {
                    fieldsBuilder.Append(i < _abstractClass._fields.Count() - 1
                        ? $"{_abstractClass._fields.ElementAt(i).Type} {_abstractClass._fields.ElementAt(i).Name}, "
                        : $"{_abstractClass._fields.ElementAt(i).Type} {_abstractClass._fields.ElementAt(i).Name}");
                    
                    fieldsAbstractBuilder.Append(i < _abstractClass._fields.Count() - 1
                        ? $"{_abstractClass._fields.ElementAt(i).Name}, "
                        : $"{_abstractClass._fields.ElementAt(i).Name}");
                }
            }
            
            contents.Add($"\t[ProtocolId({_protocolId})]");
            contents.Add(_abstractClass == null ? $"\tpublic record {_name}({fieldsBuilder});" : $"\tpublic record {_name}({fieldsBuilder}) : {_abstractClass._name}({fieldsAbstractBuilder});");
            contents.Add("}");

            return contents;
        }

        public override bool Equals(object? obj)
        {
            if (obj == null)
                return false;
            
            var comparedObject = (CsFile)obj;
            
            if (_name != comparedObject._name)
                return false;

            if (_protocolId != comparedObject._protocolId)
                return false;

            return (_fields.SequenceEqual(comparedObject._fields));
        }
    }
    public record Field(string Type, string Name, string Value = "");
    
}