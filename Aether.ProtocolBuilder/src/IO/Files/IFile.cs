﻿using System.Collections.Generic;
using System.IO;

namespace Aether.ProtocolBuilder.IO.Files
{
    public interface IFile
    {
        IEnumerable<string> Build();
    }
    
    public record FileDefinition(string Name, string Content, string Path);
}