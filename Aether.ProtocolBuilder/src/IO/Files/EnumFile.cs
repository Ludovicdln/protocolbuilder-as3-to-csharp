﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Aether.ProtocolBuilder.IO.Files
{
    public class EnumFile : IFile
    {
        private string _name;
        private IEnumerable<Field> _enums;
        public EnumFile(string name, IEnumerable<Field> enums) => (_name, _enums) = (name, enums);
        
        public IEnumerable<string> Build()
        {
            List<string> contents = new List<string>();
            
            // NAMESPACE
            string nameSpace = !IsConstant() ? "namespace Aether.Protocol.Enums" : "namespace Aether.Protocol";
            contents.Add(nameSpace);
            contents.Add("{");
            
            // ENUM OR CONST
            contents.Add(!IsConstant() ? $"\tpublic enum {_name}" : $"\tpublic static class {_name}");
            contents.Add("\t{");
            contents.AddRange(!IsConstant() ? _enums.Select((t, i) => i < _enums.Count() - 1 ? $"\t\t{t.Name} {t.Value}," : $"\t\t{t.Name} {t.Value}") : _enums.Select((t) => $"\t\tpublic const {t.Type} {t.Name} = {t.Value};"));
            contents.Add("\t}");
            contents.Add("}");

            return contents;
        }
        private bool IsConstant() => _name.ToLower().Contains("constants");

        public override bool Equals(object? obj)
        {
            if (obj == null)
                return false;
            
            var comparedObject = (EnumFile)obj;
            
            if (_name != comparedObject._name)
                return false;
            
            return (_enums.SequenceEqual(comparedObject._enums));
        }
    }
}