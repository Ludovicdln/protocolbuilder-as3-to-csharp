﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Aether.ProtocolBuilder.IO.Files;

namespace Aether.ProtocolBuilder.IO.Parser
{
    public class AsParser : IParser
    {
        public bool TryParse(FileDefinition fileDefinition, out IFile file)
        {
            var (name, content, path) = fileDefinition;
            
            if (string.IsNullOrEmpty(content)) {
                throw new Exception("content is empty");
            }
            
            var protocolId = GetProtocolId(content);
            
            var enums = GetEnums(content);

            if (protocolId == -1 && enums.Any()) {
                
                file = new EnumFile(name, enums);
                
                return true;
            }
            
            var fields = GetFields(content);

            var abstractFile = GetAbstractClass(content);

            string abstractFilePath = $"{fileDefinition.Path}\\{abstractFile}.as";

            if (File.Exists(abstractFilePath))
            {
                var abstractFileDefinition = new FileDefinition(abstractFile, File.ReadAllText(abstractFilePath),Path.GetDirectoryName(abstractFilePath));
                    
                TryParse(abstractFileDefinition, out var abstractClass);

                file = new CsFile(name, protocolId, fields, (CsFile)abstractClass);

                return true;
            }
            
            file = new CsFile(name, protocolId, fields);
            
            return true;
        }
        public int GetProtocolId(string content)
        {
            var match = Match(content, @"protocolId:uint\s=\s(\d*)");

            if (match.Count <= 0)
                return -1;

            int.TryParse(match[0].Groups[1].Value, out var result);

            return result;
        }
     
        public string GetAbstractClass(string content)
        {
            var match = Match(content, @"extends\s(\w+)");
            return match.Count <= 0 ? null : match[0].Groups[1].Value;
        }
        
        public IEnumerable<Field> GetFields(string content)
        {
            var matches = Match(content, @"public var\s(\w*):(\w+)");

            Field[] fields = new Field[matches.Count];

            for (var i = 0; i < matches.Count; i++)
            {
                var field = matches[i].Groups[1].Value;
                
                var typeMatches = Match(content, $@"{field} = \w+.(read\w+)");
                
                if (typeMatches.Count <= 0)
                {
                    var type = matches[i].Groups[2].Value;
                    
                    var vectorType = Match(content, $@"{field}:Vector.<(\w+)>");

                    if (vectorType.Count <= 0) {
                        fields[i] = new Field(type, ToFirstCharUpper(field));
                    }
                    else {
                        fields[i] = new Field($"{vectorType[0].Groups[1].Value}[]", ToFirstCharUpper(field));
                    }
                    continue;
                }
                
                for (int j = 0; j < typeMatches.Count; j++)
                {
                    string type = GetCSharpType(typeMatches[j].Groups[1].Value);
                    
                    fields[i] = new Field(type, ToFirstCharUpper(field));
                }
            }

            return fields;
        }
        
        public IEnumerable<Field> GetEnums(string content)
        {
            var matches = Match(content,@"static const\s(\w+):(\w+)\s=\s(\d+)");

            Field[] enums = new Field[matches.Count];

            for (int i = 0; i < matches.Count; i++)
            {
                string field = matches[i].Groups[1].Value;
                string type = matches[i].Groups[2].Value;
                string value = matches[i].Groups[3].Value;

                enums[i] = new Field(type, field, value);
            }

            return enums;
        }
        private static MatchCollection Match(string text, string pattern) => Regex.Matches(text, pattern, RegexOptions.Multiline);
        private static string ToFirstCharUpper(string str) => char.ToUpper(str[0]) + str.Substring(1);
        private static string GetCSharpType(string asType) =>
            asType.ToLower() switch
            {
                "readbyte" => "byte",
                "readshort" => "short",
                "readvarushort" => "ushort",
                "readutf" => "string",
                "readdouble" => "double",
                "readlong" => "long",
                "readint" => "int",
                "readboolean" => "boolean",
                _ => null
            };
    }
}