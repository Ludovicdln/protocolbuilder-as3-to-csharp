﻿using System.Threading.Tasks;
using Aether.ProtocolBuilder.IO.Files;

namespace Aether.ProtocolBuilder.IO.Parser
{
    public interface IParser
    {
        bool TryParse(FileDefinition fileDefinition, out IFile file);
    }

}