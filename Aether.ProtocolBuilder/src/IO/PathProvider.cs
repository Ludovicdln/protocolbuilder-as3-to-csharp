﻿namespace Aether.ProtocolBuilder.IO
{
    public class PathProvider
    {
        private string _basePath;
        private string _pathMessages;
        private string _pathEnums;
        private string _pathTypes;
        
        public PathProvider(string basePath, string pathMessages, string pathEnums, string pathTypes)
            => (_basePath, _pathMessages, _pathEnums, _pathTypes) = (basePath, pathMessages, pathEnums, pathTypes);
        
        public string Base => _basePath;
        public string Messages => $"{_basePath}{_pathMessages}";
        public string Enums => $"{_basePath}{_pathEnums}";
        public string Types => $"{_basePath}{_pathTypes}";
    }
}