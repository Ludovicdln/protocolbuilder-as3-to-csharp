﻿using System;
using System.IO;
using System.Linq;
using Aether.ProtocolBuilder.IO;
using Aether.ProtocolBuilder.IO.Files;
using Aether.ProtocolBuilder.IO.Parser;
using Aether.ProtocolBuilder.Services;
using Aether.XML;

namespace Aether.ProtocolBuilder.Core
{
    public class Startup
    {
        private static ServiceLogger _loggerService = new ServiceLogger();
        public static void Run()
        {
            var pathConfig = ".\\Config.xml";
            
            if (!File.Exists(pathConfig))
            {
                _loggerService.GetLogger().LogError($"{Path.GetFileName(pathConfig)} doesn't exist");
                return;
            }
            
            var xmlConfig = new MyXmlDocument(pathConfig);

            var xmlReader = new MyXmlReader(xmlConfig);

            bool result = xmlReader.TryRead(out var nodes);

            if (!result || nodes?.Count() <= 0)
            {
                _loggerService.GetLogger().LogError($"Can't get nodes from {Path.GetFileName(pathConfig)}");
                return;
            }

            var pathProvider = new PathProvider(nodes.ElementAt(0).Value, nodes.ElementAt(1).Value,
                nodes.ElementAt(2).Value, nodes.ElementAt(3).Value);
            
            _loggerService.GetLogger().LogInformation("What do you want build to csharp ? \n 1 : Messages \n 2 : Enums \n 3 : Types \n 4 : All");

            int.TryParse(Console.ReadLine(), out int choice);

            var path = GetPathSelected(choice, pathProvider);
            
            LoopFiles(new AsParser(), path, ".\\cs_generated");
            
            _loggerService.GetLogger().LogWarning("all files generated");
        }
        private static void LoopFiles(IParser parser, string path, string csPath)
        {
            var pathFiles = Directory.GetFiles(path);
            
            var directories = Directory.GetDirectories(path);
            
            foreach (var directory in directories)
            {
                if (!Directory.Exists($"{csPath}\\{directory}"))
                    Directory.CreateDirectory($"{csPath}\\{directory}");
            }

            foreach (var pathFile in pathFiles)
            {
                var fileDefinition = new FileDefinition(Path.GetFileNameWithoutExtension(pathFile), File.ReadAllText(pathFile), Path.GetDirectoryName(pathFile));
                parser.TryParse(fileDefinition, out var file);

                var content = file.Build();
                
                var directoryName = Path.GetDirectoryName(pathFile)?.Remove(0, 1);
                var csFileName = fileDefinition.Name;
                var pathFileGenerated = $"{csPath}{directoryName}\\{csFileName}.cs";
                
                File.WriteAllLines($"{pathFileGenerated}", content);
                _loggerService.GetLogger().LogInformation($"file {pathFileGenerated} generated");
            }
            
            foreach (var directory in directories) {
                LoopFiles(parser, directory, csPath);
            }
        }
        private static string GetPathSelected(int choice, PathProvider pathProvider)
        {
            return choice switch
            {
                1 => pathProvider.Messages,
                2 => pathProvider.Enums,
                3 => pathProvider.Types,
                _ => pathProvider.Base
            };
        }
        
    }
}