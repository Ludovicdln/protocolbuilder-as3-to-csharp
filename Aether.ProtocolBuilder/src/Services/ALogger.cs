﻿using Microsoft.Extensions.Logging;

namespace Aether.ProtocolBuilder.Services
{
    public class ALogger
    {
        private readonly ILogger _logger;
        public ALogger(ILogger<ALogger> logger)
        {
            this._logger = logger;
        }

        public void LogInformation(string message)
        {
            _logger.LogInformation(message);
        }

        public void LogWarning(string message)
        {
            _logger.LogWarning(message);
        }

        public void LogError(string message)
        {
            _logger.LogError(message);
        }
    }
}
