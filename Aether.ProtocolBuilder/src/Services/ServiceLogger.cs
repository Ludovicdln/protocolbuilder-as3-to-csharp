﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Aether.ProtocolBuilder.Services
{
    public class ServiceLogger
    {
        private ServiceCollection _serviceCollection;
        public ServiceLogger()
        {
            _serviceCollection = new ServiceCollection();
            ConfigureServices(_serviceCollection);
        }
        public ALogger GetLogger()
        {
            var serviceProvider = _serviceCollection.BuildServiceProvider();
            return serviceProvider.GetService<ALogger>();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(builder =>
            {
                builder.AddConsole();

            }).AddTransient<ALogger>();
        }
    }
}
