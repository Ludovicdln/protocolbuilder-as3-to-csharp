using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Aether.ProtocolBuilder.IO.Files;
using Aether.ProtocolBuilder.IO.Parser;
using Newtonsoft.Json.Serialization;
using Xunit;

namespace Aether.ProtocolBuilder.Test
{
    public class UnitTest1
    {
        [Fact]
        public void TryGetProtocolId()
        {
            var parser = new AsParser();
            var parserEnum = new AsParser();

            int protoM = parser.GetProtocolId("public static const protocolId:uint = 76;");
            int protoEnum = parserEnum.GetProtocolId("");
            
            Assert.Equal(76, protoM);
            Assert.Equal(-1, protoEnum);
        }
        
        [Fact]
        public void TryGetFields()
        {
            var parser = new AsParser();

            var parserEnum = new AsParser();

            var parserVector = new AsParser();

            var fields = parser.GetFields(@"public var maxTaxCollectorsCount:uint = 0;
                                                public var taxCollectorsCount:uint = 0;
                                        this.maxTaxCollectorsCount = param1.readByte();
                                        this.taxCollectorsCount = param1.readByte();");
            var fieldsEnums = parserEnum.GetFields(@"public static const Enutrof:int = 3;
                                                    public static const Sram:int = 4;");
            var fieldsVector = parserVector.GetFields(@"public var aliases:Vector.<String>;
                                                         public var arguments:Vector.<String>;
                                                         var _loc3_:uint = param1.readUnsignedShort();
                                                         var _loc5_:uint = param1.readUnsignedShort();");
                            
            Assert.Equal(GetFields(), fields);
            Assert.Equal(new List<Field>(), fieldsEnums);
            Assert.Equal(GetVectorFields(), fieldsVector);
        }
        
        [Fact]
        public void TryGetEnumsOrConsts()
        {
            var parserConst = new AsParser();

            var parserEnum = new AsParser();

            var enumConst = parserConst.GetEnums(@"public static const MAP_CELL_COUNT:int = 560;    
                                                   public static const MAX_LEVEL:int = 200;     
                                                   public static const MAX_CHAT_LENGTH_CHAT:int = 256;      
                                                    public static const MIN_LOGIN_LEN:int = 3;
                                                    public static const MAX_LOGIN_LEN:int = 50;");
            var enums = parserEnum.GetEnums(@"public static const Enutrof:int = 3;
                                                    public static const Sram:int = 4;");

            Assert.Equal(GetConst(), enumConst);
            Assert.Equal(GetEnums(), enums);
        }
        
        [Fact]
        public void TryGetAbstractClass()
        {
            var parserNetwork = new AsParser();
            var parserAbstract = new AsParser();

            var abstractNetwork = parserNetwork.GetAbstractClass("public class GuildLevelUpMessage extends NetworkMessage implements INetworkMessage");
            var abstractAdmin = parserAbstract.GetAbstractClass("public class AdminQuietCommandMessage extends AdminCommandMessage implements INetworkMessage");
            
            Assert.Equal("NetworkMessage", abstractNetwork);
            Assert.Equal("AdminCommandMessage", abstractAdmin);
        }
        
        [Fact]
        public void TryBuildCsFiles()
        {
            string path1 = ".\\protocol\\messages\\authorized\\AdminQuietCommandMessage.as";
            string path2 = ".\\protocol\\messages\\authorized\\ConsoleCommandsListMessage.as";
            
            var contentAdminQuiet = File.ReadAllText(path1);
            
            string fileNameAdminQuiet = Path.GetFileNameWithoutExtension(path1);
            
            var contentConsole = File.ReadAllText(path2);
            string fileNameConsole = Path.GetFileNameWithoutExtension(path2);

            var parserAdminQuiet = new AsParser();
            parserAdminQuiet.TryParse(new FileDefinition(fileNameAdminQuiet, contentAdminQuiet, Path.GetDirectoryName(path1)), out var fileAdminQuiet);
            
            var parserAdmin = new AsParser();
            parserAdmin.TryParse(new FileDefinition(fileNameConsole, contentConsole, Path.GetDirectoryName(path2)), out var fileConsole);

            Assert.Equal(GetCsFiles().ElementAt(0), (CsFile)fileAdminQuiet);
            Assert.Equal(GetCsFiles().ElementAt(1), (CsFile)fileConsole);
        }

        private static IEnumerable<CsFile> GetCsFiles()
        {
            yield return new CsFile("AdminQuietCommandMessage", 5662, new Field[0], new CsFile("AdminCommandMessage", 76, new Field[1]
            {
                new Field("string", "content")
            }));
            yield return new CsFile("ConsoleCommandsListMessage", 6127, new Field[3]
            {
                new Field("string[]", "Aliases"),
                new Field("string[]", "Arguments"),
                new Field("string[]", "Descriptions")
            });
        }

        private static IEnumerable<Field> GetFields()
        {
            yield return new Field("byte", "MaxTaxCollectorsCount");
            yield return new Field("byte", "TaxCollectorsCount");
        }
        
        private static IEnumerable<Field> GetVectorFields()
        {
            yield return new Field("string[]", "Aliases");
            yield return new Field("string[]", "Arguments");
        }
        
        private static IEnumerable<Field> GetEnums()
        {
            yield return new Field("int", "Enutrof", "3");
            yield return new Field("int","Sram", "4");
        }
        
        private static IEnumerable<Field> GetConst()
        {
            yield return new Field("int", "MAP_CELL_COUNT", "560");
            yield return new Field("int","MAX_LEVEL", "200");
            yield return new Field("int","MAX_CHAT_LENGTH_CHAT", "256");
            yield return new Field("int","MIN_LOGIN_LEN", "3");
            yield return new Field("int","MAX_LOGIN_LEN", "50");

        }
    }
}