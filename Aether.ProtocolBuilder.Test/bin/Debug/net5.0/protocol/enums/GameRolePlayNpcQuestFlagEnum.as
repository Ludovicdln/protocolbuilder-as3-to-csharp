package com.ankamagames.dofus.network.enums
{
   [Trusted]
   public class GameRolePlayNpcQuestFlagEnum
   {
      
      public static const QUEST_FLAG_NONE:uint = 0;
      
      public static const QUEST_FLAG_VALID:uint = 1;
      
      public static const QUEST_FLAG_START:uint = 2;
       
      
      public function GameRolePlayNpcQuestFlagEnum()
      {
         super();
      }
   }
}
