package com.ankamagames.dofus.network.messages.game.context.roleplay.party
{
   import com.ankamagames.jerakine.network.INetworkMessage;
   import flash.utils.ByteArray;
   import flash.utils.IDataInput;
   import flash.utils.IDataOutput;
   
   [Trusted]
   public class PartyFollowMemberRequestMessage extends AbstractPartyMessage implements INetworkMessage
   {
      
      public static const protocolId:uint = 5577;
       
      
      private var _isInitialized:Boolean = false;
      
      public var playerId:uint = 0;
      
      public function PartyFollowMemberRequestMessage()
      {
         super();
      }
      
      override public function get isInitialized() : Boolean
      {
         return super.isInitialized && this._isInitialized;
      }
      
      override public function getMessageId() : uint
      {
         return 5577;
      }
      
      public function initPartyFollowMemberRequestMessage(param1:uint = 0, param2:uint = 0) : PartyFollowMemberRequestMessage
      {
         super.initAbstractPartyMessage(param1);
         this.playerId = param2;
         this._isInitialized = true;
         return this;
      }
      
      override public function reset() : void
      {
         super.reset();
         this.playerId = 0;
         this._isInitialized = false;
      }
      
      override public function pack(param1:IDataOutput) : void
      {
         var _loc2_:ByteArray = new ByteArray();
         this.serialize(_loc2_);
         writePacket(param1,this.getMessageId(),_loc2_);
      }
      
      override public function unpack(param1:IDataInput, param2:uint) : void
      {
         this.deserialize(param1);
      }
      
      override public function serialize(param1:IDataOutput) : void
      {
         this.serializeAs_PartyFollowMemberRequestMessage(param1);
      }
      
      public function serializeAs_PartyFollowMemberRequestMessage(param1:IDataOutput) : void
      {
         super.serializeAs_AbstractPartyMessage(param1);
         if(this.playerId < 0)
         {
            throw new Error("Forbidden value (" + this.playerId + ") on element playerId.");
         }
         param1.writeInt(this.playerId);
      }
      
      override public function deserialize(param1:IDataInput) : void
      {
         this.deserializeAs_PartyFollowMemberRequestMessage(param1);
      }
      
      public function deserializeAs_PartyFollowMemberRequestMessage(param1:IDataInput) : void
      {
         super.deserialize(param1);
         this.playerId = param1.readInt();
         if(this.playerId < 0)
         {
            throw new Error("Forbidden value (" + this.playerId + ") on element of PartyFollowMemberRequestMessage.playerId.");
         }
      }
   }
}
