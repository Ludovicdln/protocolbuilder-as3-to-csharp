package com.ankamagames.dofus.network.messages.game.approach
{
   import com.ankamagames.jerakine.network.INetworkMessage;
   import com.ankamagames.jerakine.network.NetworkMessage;
   import flash.utils.ByteArray;
   import flash.utils.IDataInput;
   import flash.utils.IDataOutput;
   
   [Trusted]
   public class ServerOptionalFeaturesMessage extends NetworkMessage implements INetworkMessage
   {
      
      public static const protocolId:uint = 6305;
       
      
      private var _isInitialized:Boolean = false;
      
      public var features:Vector.<uint>;
      
      public function ServerOptionalFeaturesMessage()
      {
         this.features = new Vector.<uint>();
         super();
      }
      
      override public function get isInitialized() : Boolean
      {
         return this._isInitialized;
      }
      
      override public function getMessageId() : uint
      {
         return 6305;
      }
      
      public function initServerOptionalFeaturesMessage(param1:Vector.<uint> = null) : ServerOptionalFeaturesMessage
      {
         this.features = param1;
         this._isInitialized = true;
         return this;
      }
      
      override public function reset() : void
      {
         this.features = new Vector.<uint>();
         this._isInitialized = false;
      }
      
      override public function pack(param1:IDataOutput) : void
      {
         var _loc2_:ByteArray = new ByteArray();
         this.serialize(_loc2_);
         writePacket(param1,this.getMessageId(),_loc2_);
      }
      
      override public function unpack(param1:IDataInput, param2:uint) : void
      {
         this.deserialize(param1);
      }
      
      public function serialize(param1:IDataOutput) : void
      {
         this.serializeAs_ServerOptionalFeaturesMessage(param1);
      }
      
      public function serializeAs_ServerOptionalFeaturesMessage(param1:IDataOutput) : void
      {
         param1.writeShort(this.features.length);
         var _loc2_:uint = 0;
         while(_loc2_ < this.features.length)
         {
            if(this.features[_loc2_] < 0)
            {
               throw new Error("Forbidden value (" + this.features[_loc2_] + ") on element 1 (starting at 1) of features.");
            }
            param1.writeShort(this.features[_loc2_]);
            _loc2_++;
         }
      }
      
      public function deserialize(param1:IDataInput) : void
      {
         this.deserializeAs_ServerOptionalFeaturesMessage(param1);
      }
      
      public function deserializeAs_ServerOptionalFeaturesMessage(param1:IDataInput) : void
      {
         var _loc4_:uint = 0;
         var _loc2_:uint = param1.readUnsignedShort();
         var _loc3_:uint = 0;
         while(_loc3_ < _loc2_)
         {
            _loc4_ = param1.readShort();
            if(_loc4_ < 0)
            {
               throw new Error("Forbidden value (" + _loc4_ + ") on elements of features.");
            }
            this.features.push(_loc4_);
            _loc3_++;
         }
      }
   }
}
