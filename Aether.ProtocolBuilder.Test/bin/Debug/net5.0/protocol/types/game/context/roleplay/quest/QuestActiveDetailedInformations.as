package com.ankamagames.dofus.network.types.game.context.roleplay.quest
{
   import com.ankamagames.jerakine.network.INetworkType;
   import flash.utils.IDataInput;
   import flash.utils.IDataOutput;
   
   public class QuestActiveDetailedInformations extends QuestActiveInformations implements INetworkType
   {
      
      public static const protocolId:uint = 382;
       
      
      public var stepId:uint = 0;
      
      public var objectivesIds:Vector.<uint>;
      
      public var objectivesStatus:Vector.<Boolean>;
      
      public function QuestActiveDetailedInformations()
      {
         this.objectivesIds = new Vector.<uint>();
         this.objectivesStatus = new Vector.<Boolean>();
         super();
      }
      
      override public function getTypeId() : uint
      {
         return 382;
      }
      
      public function initQuestActiveDetailedInformations(param1:uint = 0, param2:uint = 0, param3:Vector.<uint> = null, param4:Vector.<Boolean> = null) : QuestActiveDetailedInformations
      {
         super.initQuestActiveInformations(param1);
         this.stepId = param2;
         this.objectivesIds = param3;
         this.objectivesStatus = param4;
         return this;
      }
      
      override public function reset() : void
      {
         super.reset();
         this.stepId = 0;
         this.objectivesIds = new Vector.<uint>();
         this.objectivesStatus = new Vector.<Boolean>();
      }
      
      override public function serialize(param1:IDataOutput) : void
      {
         this.serializeAs_QuestActiveDetailedInformations(param1);
      }
      
      public function serializeAs_QuestActiveDetailedInformations(param1:IDataOutput) : void
      {
         super.serializeAs_QuestActiveInformations(param1);
         if(this.stepId < 0)
         {
            throw new Error("Forbidden value (" + this.stepId + ") on element stepId.");
         }
         param1.writeShort(this.stepId);
         param1.writeShort(this.objectivesIds.length);
         var _loc2_:uint = 0;
         while(_loc2_ < this.objectivesIds.length)
         {
            if(this.objectivesIds[_loc2_] < 0)
            {
               throw new Error("Forbidden value (" + this.objectivesIds[_loc2_] + ") on element 2 (starting at 1) of objectivesIds.");
            }
            param1.writeShort(this.objectivesIds[_loc2_]);
            _loc2_++;
         }
         param1.writeShort(this.objectivesStatus.length);
         var _loc3_:uint = 0;
         while(_loc3_ < this.objectivesStatus.length)
         {
            param1.writeBoolean(this.objectivesStatus[_loc3_]);
            _loc3_++;
         }
      }
      
      override public function deserialize(param1:IDataInput) : void
      {
         this.deserializeAs_QuestActiveDetailedInformations(param1);
      }
      
      public function deserializeAs_QuestActiveDetailedInformations(param1:IDataInput) : void
      {
         var _loc6_:uint = 0;
         var _loc7_:Boolean = false;
         super.deserialize(param1);
         this.stepId = param1.readShort();
         if(this.stepId < 0)
         {
            throw new Error("Forbidden value (" + this.stepId + ") on element of QuestActiveDetailedInformations.stepId.");
         }
         var _loc2_:uint = param1.readUnsignedShort();
         var _loc3_:uint = 0;
         while(_loc3_ < _loc2_)
         {
            _loc6_ = param1.readShort();
            if(_loc6_ < 0)
            {
               throw new Error("Forbidden value (" + _loc6_ + ") on elements of objectivesIds.");
            }
            this.objectivesIds.push(_loc6_);
            _loc3_++;
         }
         var _loc4_:uint = param1.readUnsignedShort();
         var _loc5_:uint = 0;
         while(_loc5_ < _loc4_)
         {
            _loc7_ = param1.readBoolean();
            this.objectivesStatus.push(_loc7_);
            _loc5_++;
         }
      }
   }
}
