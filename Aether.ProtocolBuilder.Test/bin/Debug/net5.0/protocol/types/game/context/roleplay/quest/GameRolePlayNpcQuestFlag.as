package com.ankamagames.dofus.network.types.game.context.roleplay.quest
{
   import com.ankamagames.jerakine.network.INetworkType;
   import flash.utils.IDataInput;
   import flash.utils.IDataOutput;
   
   public class GameRolePlayNpcQuestFlag implements INetworkType
   {
      
      public static const protocolId:uint = 384;
       
      
      public var questId:uint = 0;
      
      public var questFlag:uint = 0;
      
      public function GameRolePlayNpcQuestFlag()
      {
         super();
      }
      
      public function getTypeId() : uint
      {
         return 384;
      }
      
      public function initGameRolePlayNpcQuestFlag(param1:uint = 0, param2:uint = 0) : GameRolePlayNpcQuestFlag
      {
         this.questId = param1;
         this.questFlag = param2;
         return this;
      }
      
      public function reset() : void
      {
         this.questId = 0;
         this.questFlag = 0;
      }
      
      public function serialize(param1:IDataOutput) : void
      {
         this.serializeAs_GameRolePlayNpcQuestFlag(param1);
      }
      
      public function serializeAs_GameRolePlayNpcQuestFlag(param1:IDataOutput) : void
      {
         if(this.questId < 0)
         {
            throw new Error("Forbidden value (" + this.questId + ") on element questId.");
         }
         param1.writeShort(this.questId);
         param1.writeByte(this.questFlag);
      }
      
      public function deserialize(param1:IDataInput) : void
      {
         this.deserializeAs_GameRolePlayNpcQuestFlag(param1);
      }
      
      public function deserializeAs_GameRolePlayNpcQuestFlag(param1:IDataInput) : void
      {
         this.questId = param1.readShort();
         if(this.questId < 0)
         {
            throw new Error("Forbidden value (" + this.questId + ") on element of GameRolePlayNpcQuestFlag.questId.");
         }
         this.questFlag = param1.readByte();
         if(this.questFlag < 0)
         {
            throw new Error("Forbidden value (" + this.questFlag + ") on element of GameRolePlayNpcQuestFlag.questFlag.");
         }
      }
   }
}
