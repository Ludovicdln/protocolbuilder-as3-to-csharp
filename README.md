# Protocol Builder ~ AS3 -> C#

<div style="text-align:center"><p align="center"><img src="https://zupimages.net/up/20/52/01bl.png"></img></p></div>

###  it's used to generate csharp files from action script files, check out the netcoreapp5.0 directory

<div style="text-align:center"><p align="center"><img src="https://zupimages.net/up/20/52/0a16.png"></img></p></div>

```csharp

namespace Aether.ProtocolBuilder.IO.Parser
{
    public interface IParser
    {
        bool TryParse(FileDefinition fileDefinition, out IFile file);
    }
}

```

```csharp

namespace Aether.ProtocolBuilder.IO.Files
{
    public interface IFile
    {
        IEnumerable<string> Build();
    }
    
}	

```

